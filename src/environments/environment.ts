// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBEYLfn-7VRItsyU974JUI5_ATEd208xAY",
    authDomain: "ventasromeroyandry.firebaseapp.com",
    databaseURL: "https://ventasromeroyandry.firebaseio.com",
    projectId: "ventasromeroyandry",
    storageBucket: "ventasromeroyandry.appspot.com",
    messagingSenderId: "901888645748",
    appId: "1:901888645748:web:597d2ba21880b082e54f21"
  },
  googleWebClientId: '901888645748-bg3nnc7jfpbea3hepilig9oks7f7btk0.apps.googleusercontent.com'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
