import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  verificado: boolean;
  email: string;
  password: string;

  constructor(private authService: AuthService, public router: Router,  private navCtrl: NavController) { }

  ngOnInit() {
  }

  doLogin(){
    this.authService.login(this.email, this.password).then(usr => {
      this.authService.getUserAuth().subscribe(user => {
        if (usr && user.emailVerified){
          console.log("Esto es el usr");
          console.log(usr);
          console.log("Esto es el user");
          console.log(user);
          if(user){
            this.router.navigate(['/tab/home']);
          }else{
            this.router.navigate(['/tab/home']);
          }
          
        }else if (usr){
          this.navCtrl.navigateForward(['/verification-page', this.email]);
        }
      });
    }).catch(error => {
      alert("los datos son incorrectos");
    });
  }

  async loginGoogle2(){
    let conexion = await this.authService.googleLogin();
    if (conexion === undefined){
      this.router.navigate(['home']);
    }else{
      alert("Algo salio mal!" + JSON.stringify(conexion));

    }
  }

  loginGoogle(){
    this.authService.loginwithgoogle().then(() => {
      this.router.navigate(['/home']);
    }).catch(error => {
      alert("Algo salio mal!");
      console.log(error);
    });
  }

}
