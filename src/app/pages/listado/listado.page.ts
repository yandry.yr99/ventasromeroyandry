import { Component, OnInit, Input } from '@angular/core';
import { CelularesService } from 'src/app/services/celulares.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.page.html',
  styleUrls: ['./listado.page.scss'],
})
export class ListadoPage implements OnInit {

  public veters: any = [];
  uidVeterinarias = 'nfNJKos4wIPh1LCqTG3p';
  categoriaVeterinarias = 'celularesColeccion';

  constructor(private celularesService:CelularesService,public router: Router, private activatedroute: ActivatedRoute, private navCtrl: NavController) { }

  ngOnInit() {
    this.celularesService.getListarServicios(this.uidVeterinarias, this.categoriaVeterinarias).subscribe( vets => {
      console.log(vets);
      this.veters = vets;
      console.log(this.veters[1]);
    });
  }

  comprar(uid:string){
    this.navCtrl.navigateRoot(['carrito',uid])
  }

  


  

}
