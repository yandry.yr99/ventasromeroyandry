import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { NavController } from '@ionic/angular';
import { CelularesService } from 'src/app/services/celulares.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public servicios: any = [];

  constructor(private celularesService: CelularesService, private navCtrl: NavController) {}

  ngOnInit() {
    this.celularesService.getServicios().subscribe(serv =>{
      this.servicios = serv;
    }) 
  }

  click(){
    this.navCtrl.navigateRoot(['listado']);
  }

}
