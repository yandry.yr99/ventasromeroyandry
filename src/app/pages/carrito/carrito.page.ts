import { Component, OnInit } from '@angular/core';
import { CelularesService } from 'src/app/services/celulares.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.page.html',
  styleUrls: ['./carrito.page.scss'],
})
export class CarritoPage implements OnInit {

  veterinaria: any;
  uidVeterinarias = 'nfNJKos4wIPh1LCqTG3p';
  categoriaVeterinarias = 'celularesColeccion';

  constructor(private celularService: CelularesService, private route: ActivatedRoute,public router: Router) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.celularService.getServicioById(this.uidVeterinarias, this.categoriaVeterinarias,id).then(result => {
      this.veterinaria = result;
      console.log(this.veterinaria);
    });
  }

}
