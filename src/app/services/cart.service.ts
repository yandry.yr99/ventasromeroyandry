import { Injectable } from '@angular/core';

export interface Product{
  id:number;
  name: string;
  price: number;
  amount: number;
}
@Injectable({
  providedIn: 'root'
})
export class CartService {

  

  constructor() { }
}
