import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Servicio } from '../interfaces/servicio';
import { map, first } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class CelularesService {

    constructor(private db: AngularFirestore) { }

    getServicios(){
        return this.db.collection('lista-productos').snapshotChanges().pipe(map(servs => {
          return servs.map(serv => {
            const data = serv.payload.doc.data() as Servicio;
            data.uid = serv.payload.doc.id;
            return data;
          });
        }));
      }

    getListarServicios(uid: string, categoria: string){
        return this.db.collection('lista-productos').doc(uid).collection(categoria).snapshotChanges().pipe(map(servs => {
          return servs.map(serv => {
            const data = serv.payload.doc.data() as Servicio;
            data.uid = serv.payload.doc.id;
            return data;
          });
        }));
      }
    
      async getServicioById(uid: string, categoria: string, uidServicio: string): Promise<Servicio> {
        try {
            let aux: any = await this.db.collection('lista-productos').doc(uid).collection(categoria,
                ref => ref.where('uid', '==', uidServicio))
                          .valueChanges().pipe(first()).toPromise().then(doc => {
                              return doc;
                          }).catch(error => {
                              throw error;
                          });
            if (aux.length === 0) {
                return undefined;
            }
            return aux[0];
        } catch (error) {
          console.error('Error', error);
          throw error;
        }
      }
}