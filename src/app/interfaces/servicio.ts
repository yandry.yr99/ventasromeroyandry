export interface Servicio {
    titulo: string;
    descripcion: string;
    precio: number;
    uid: string;
    imagenurl: string;
}
